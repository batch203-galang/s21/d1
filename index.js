// console.log("Hello World!");

// We want to list the student ID of all graduating student of the class.

let studentNumberA = "2020-1923";
let studentNumberB = "2020-1924";
let studentNumberC = "2020-1925";
let studentNumberD = "2020-1926";
let studentNumberE = "2020-1927";

// We can simply write the code above like this in array:
let studentNumbers = ["2020-1923","2020-1924","2020-1925","2020-1926","2020-1927"];

// [SECTION] Arrays

/*
	- Arrays are used to store multiple related values in a single variable.
	- They are declared using square brackets ([]) also known as "Array Literals"
	- Arrays it also provides access to a number of functions/methods that help in manipulation array.
		- Methods are used to manipulate information stored within the same object.
		- Array are also objects which is another data type.
		- The main difference of arrays with object is that it contains information in a form of "list" unlike objects which uses "properties" (key-value pair).

		- Syntax:
			let/const arrayName = [elementA, elementB, elementC, ...., ElementNth];
*/

// Common examples of arrays
let grades = [98.5, 94.3, 89.2, 90];
let computerBrands = ["Acer","Asus","Lenovo","Neo","Redfox","Gateway","Toshiba","Fujitsu"];

console.log(grades);
console.log(computerBrands);

// Possible use of an array but it is not recommended
let mixedArr = ["John", "Doe", 12, false, null, undefined, {}];
console.log(mixedArr);

// Alternative way to write array

let myTasks = [
	"drink html",
	"eat javascript",
	"inhale css",
	"base sass"
];

console.log(myTasks);

// Creating an array with values from variables
let city = "Tokyo";
let city1 = "Manila";
let city2 = "Jakarta";

let cities = [city, city1, city2];
console.log(cities);

// Object Values; let sampleObjects = [{obj1}, {obj2}..];

// [SECTION] .length property
// ".length" property allows us to get and set the total number of items in an array.

console.log(myTasks.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);

// .length property can also be used in string. As well as methods and properties can also be used with strings.

let fullName = "Ruth Galang";
console.log(fullName.length); //spaces are counted as characters in strings.

// .length property can also set the total number of items in an array.
console.log(myTasks.length);
console.log(myTasks);

myTasks.length = myTasks.length - 1
console.log(myTasks.length);
console.log(myTasks);

// TO DELETE an specific item in an array we can employ array methods. We have only shown the logic or algorithm of the pop method.

// Another example using decrement.

cities.length--;
console.log(cities);

// We can't do the same on string.
fullName.length = fullName.length - 1;
console.log(fullName.length);
console.log(fullName);

// We can also add the length of an array.
let theBeatles = ["John","Paul","Ringo","George"];
//theBeatles.length++;
theBeatles[theBeatles.length] = "Cardo";
console.log(theBeatles);

// [SECTION] Reading from Arrays
	/*
		- Accessing array elements is one of the common task that we do with an array.
		- This can be done through the use of an array indexes.
		- Each element in an array is associated with it's own index number.
		- The first element in an array is associated with the number 0, and increasing this number by 1 for every element.
		- Array indexes it is actually refer to a memory address/location

		Array Address: 0x7ffe942bad0
		Array[0] = 0x7ffe942bad0
		Array[1] = 0x7ffe942bad4
		Array[2] = 0x7ffe942bad8

		- Syntax
			arrayName[index];
	*/

	console.log(grades[0]);
	console.log(computerBrands[3]);


	// Accessing an array element that does not exist it will return "undefined".
	console.log(grades[20]);

	let lakersLegends = ["Kobe","Shaq","Lebron","Magic","Kareem"];
	console.log(lakersLegends[1]); //Shaq
	console.log(lakersLegends[3]); //Magic

	// You can also save/store array items in another variable.
	let currentLaker = lakersLegends[2];
	console.log(currentLaker);
	
	// You can also reassign array values using the item's indices
	console.log("Array before reassignment");
	console.log(lakersLegends);

	lakersLegends[2] = "Gasol";
	console.log("Array after reassignment");
	console.log(lakersLegends);

	// Access the last element of an array
	// Since the first element of an array starts at 0, subtracting 1 to the length of an array will offset the value by one allowing us to get the last element.

	let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];
	let lastElementIndex = bullsLegends.length-1;
	console.log(bullsLegends[lastElementIndex]);
	//console.log(bullLegends[bullsLegend.length-1]);

	// Adding items into the array

	// we can add items in an array using indices.
	const newArr = [];
	console.log(newArr[0]);

	newArr[0] = "Cloud Strife";
	console.log(newArr);

	console.log(newArr[1]);
	newArr[1] = "Tifa Lockhart";
	console.log(newArr);

	// we can add items at the end of the array using the array.length

	//newArr[newArr.length-1] = "Barrett Wallace"; //reassigns the value of the last element.
	newArr[newArr.length] = "Barrett Wallace";
	console.log(newArr);

	// Looping over an array
	// You can a for loop to iterate overall items in an array.

	for(let index = 0; index < newArr.length; index++){
		// to be able to show each array items in the console.log
		console.log(newArr[index]);
	}

	// Create a program that will filter the array of numbers which are divisible by 5.

	let numArr = [5 , 12 , 30, 46, 40, 52];
	
	for(let i = 0; i < numArr.length; i++){
		if(numArr[i]%5 === 0){
			console.log(numArr[i]+ " is divisible by 5.");
		}
		else{
			console.log(numArr[i]+ " is not divisible by 5.");
		}
	}

	// [SECTION] Multidimensional Arrays
	/*
		- Multidimensional arrays are useful for storing complex data structures.
		- A practical application of this is to helo visualize/create real world objects.
		- This is frequently used to store data for mathematic computations, image processing, and record management.
		- Array within an array

	*/

	// Create chessboard 
	let chessBoard = [
		["a1","b1","c1","d1","e1","f1","g1","h1"],
		["a2","b2","c2","d2","e2","f2","g2","h2"],
		["a3","b3","c3","d3","e3","f3","g3","h3"],
		["a4","b4","c4","d4","e4","f4","g4","h4"],
		["a5","b5","c5","d5","e5","f5","g5","h5"],
		["a6","b6","c6","d6","e6","f6","g6","h6"],
		["a7","b7","c7","d7","e7","f7","g7","h7"],
		["a8","b8","c8","d8","e8","f8","g8","h8"],
		
	];

	console.table(chessBoard);

	// Access a element of a multidimensional arrays
	// Syntax: multiArr[outerArr][innerArr]
	console.log(chessBoard[3][4]);

	console.log("Pawn moves to: " +chessBoard[1][5]);
